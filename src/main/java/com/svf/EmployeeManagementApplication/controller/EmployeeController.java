package com.svf.EmployeeManagementApplication.controller;

import com.svf.EmployeeManagementApplication.model.EmployeeModel;
import com.svf.EmployeeManagementApplication.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    public List<EmployeeModel> getAll(){
//        List<EmployeeModel> employeeModels = new ArrayList<>();
//        EmployeeModel employeeModel = new EmployeeModel("John","user");
//        employeeModels.add(employeeModel);
        return employeeRepository.findAll();
    }
    @GetMapping("/employees/{id}")
    public Optional<EmployeeModel> getOne(@PathVariable Long id){
        return employeeRepository.findById(id);
    }
    @PutMapping("/employees/{id}")
    public Optional<EmployeeModel> update(@RequestBody EmployeeModel newEmployee,@PathVariable Long id){
         Optional<EmployeeModel> existingEmp = employeeRepository.findById(id);
         if(existingEmp.isPresent()){
             EmployeeModel dbEmployee = existingEmp.get();
             dbEmployee.setName(newEmployee.getName());
             dbEmployee.setRole(newEmployee.getRole());
             employeeRepository.save(dbEmployee);
         }else {
             System.out.println("Employee not found");
         }
         return employeeRepository.findById(id);
    }
    @PostMapping("/employees")
    public EmployeeModel create(@RequestBody EmployeeModel employeeModel){
        System.out.println("employee: "+employeeModel);
        return employeeRepository.save(employeeModel);
    }
    @DeleteMapping("/employees/{id}")
    public void deleteOne(@PathVariable Long id){
        employeeRepository.deleteById(id);
    }
}
