package com.svf.EmployeeManagementApplication.configuration;

import com.svf.EmployeeManagementApplication.model.EmployeeModel;
import com.svf.EmployeeManagementApplication.repository.EmployeeRepository;
import org.hibernate.tool.schema.spi.CommandAcceptanceException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class LoadDefaultData {
    @Bean
    CommandLineRunner initializeDatabase(EmployeeRepository employeeRepository){
        return args -> {
            EmployeeModel employeeModel1 = new EmployeeModel("Smith1","Dev");
            EmployeeModel employeeModel2 = new EmployeeModel("Smith1","Dev");
            EmployeeModel employeeModel3 = new EmployeeModel("Smith1","Dev");
            EmployeeModel employeeModel4 = new EmployeeModel("Smith1","Dev");
            EmployeeModel employeeModel5 = new EmployeeModel("Smith1","Dev");
            EmployeeModel employeeModel6 = new EmployeeModel("Smith1","Dev");
            employeeRepository.saveAll(List.of(employeeModel1,
                    employeeModel2,
                    employeeModel3,
                    employeeModel4,
                    employeeModel5,
                    employeeModel6));
        };
    }
}
