package com.svf.EmployeeManagementApplication.repository;

import com.svf.EmployeeManagementApplication.model.EmployeeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel,Long> {

}
